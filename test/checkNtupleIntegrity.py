import ROOT
import os
import sys

path=sys.argv[1]
corrupted=[]
for f in os.listdir(path):
    if not '.root' in f : continue
    fIn=ROOT.TFile.Open(os.path.join(path,f))
    nhits=fIn.Get('hits').GetEntriesFast()
    fIn.Close()
    if nhits>0: continue
    os.system('rm -v {}'.format(os.path.join(path,f)))

